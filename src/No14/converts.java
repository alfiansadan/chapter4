package No14;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;

public class converts {

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					converts window = new converts();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public converts() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JButton btnNewButton = new JButton("Convert");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					double num1,  ans,ans1,ans2;
					double num2 = 3000;
					double num3 = 2800;
					double num4 = 1000;
					num1=Double.parseDouble(textField.getText());
					
					ans = num2/num1;
					textField_1.setText(Double.toString(ans));
					
					ans1 = num3/num1;
					textField_2.setText(Double.toString(ans1));
					
					ans2 = num4/num1;
					textField_3.setText(Double.toString(ans2));
					
				}
				catch(Exception e1){
					
					JOptionPane.showMessageDialog(null, "Enter valid number");
			}}
		});
		btnNewButton.setBounds(335, 34, 89, 23);
		frame.getContentPane().add(btnNewButton);
		
		textField = new JTextField();
		textField.setBounds(131, 34, 182, 23);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(164, 117, 113, 29);
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(164, 168, 113, 29);
		frame.getContentPane().add(textField_2);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(164, 221, 113, 29);
		frame.getContentPane().add(textField_3);
		
		JLabel lblNewLabel = new JLabel("Enter Dollar Amount");
		lblNewLabel.setFont(new Font("Sylfaen", Font.BOLD, 14));
		lblNewLabel.setBounds(10, 0, 165, 23);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("US Dollars");
		lblNewLabel_1.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblNewLabel_1.setBounds(20, 38, 101, 14);
		frame.getContentPane().add(lblNewLabel_1);
		
		JLabel lblDisplayExchange = new JLabel("Display Exchange");
		lblDisplayExchange.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblDisplayExchange.setBounds(20, 63, 126, 14);
		frame.getContentPane().add(lblDisplayExchange);
		
		JLabel lblCanadianDollars = new JLabel("Canadian Dollars");
		lblCanadianDollars.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblCanadianDollars.setBounds(10, 124, 144, 14);
		frame.getContentPane().add(lblCanadianDollars);
		
		JLabel lblGermanMarks = new JLabel("German Marks");
		lblGermanMarks.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblGermanMarks.setBounds(10, 175, 111, 14);
		frame.getContentPane().add(lblGermanMarks);
		
		JLabel lblBritishPounds = new JLabel("British Pounds");
		lblBritishPounds.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblBritishPounds.setBounds(10, 228, 101, 14);
		frame.getContentPane().add(lblBritishPounds);
		
		JLabel lblExchangeRate = new JLabel("Exchange Rate");
		lblExchangeRate.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblExchangeRate.setBounds(164, 93, 101, 14);
		frame.getContentPane().add(lblExchangeRate);
		
		JLabel lblConvertedAmount = new JLabel("Converted Amount");
		lblConvertedAmount.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblConvertedAmount.setBounds(298, 94, 126, 14);
		frame.getContentPane().add(lblConvertedAmount);
		
		JLabel label = new JLabel("$ 3,000.00");
		label.setFont(new Font("Times New Roman", Font.PLAIN, 12));
		label.setBounds(296, 124, 101, 14);
		frame.getContentPane().add(label);
		
		JLabel label_1 = new JLabel("2.800,00");
		label_1.setFont(new Font("Times New Roman", Font.PLAIN, 12));
		label_1.setBounds(296, 175, 101, 14);
		frame.getContentPane().add(label_1);
		
		JLabel label_2 = new JLabel("1,000.00");
		label_2.setFont(new Font("Times New Roman", Font.PLAIN, 12));
		label_2.setBounds(296, 228, 101, 14);
		frame.getContentPane().add(label_2);
	}
}
