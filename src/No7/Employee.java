package No7;

public class Employee {
	int EMP_ID;
	String EMP_Name;
	String gender;
	String city;
	String ADDRESS;
	String birthdate;
	
	public Employee() {
		super();
	}
	public Employee(int eMP_ID, String eMP_Name, String gender, String city,String ADDRESS, String birthdate) {
		
		EMP_ID = eMP_ID;
		EMP_Name = eMP_Name;
		this.gender = gender;
		this.city = city;
		this.ADDRESS = ADDRESS;
		this.birthdate = birthdate;
	}
	public int getEMP_ID() {
		return EMP_ID;
	}
	public void setEMP_ID(int eMP_ID) {
		EMP_ID = eMP_ID;
	}
	public String getEMP_Name() {
		return EMP_Name;
	}
	public String getADDRESS() {
		return ADDRESS;
	}
	public void setADDRESS(String aDDRESS) {
		ADDRESS = aDDRESS;
	}
	public void setEMP_Name(String eMP_Name) {
		EMP_Name = eMP_Name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getBirthdate() {
		return birthdate;
	}
	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}
	
	@Override
	public String toString() {
		return "Employee [EMP_ID="+ EMP_ID +", EMP_NAME ="+EMP_Name+", Gender="+gender+", Birthdate= "+birthdate+
				", Address = "+ ADDRESS+ ", City = "+ city +"]";
	}
	
}
