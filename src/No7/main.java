package No7;

import java.util.ArrayList;
import java.util.List;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.stream.Collectors;


public class main {


	static List<Employee> Employee;
	static List<Employee> employeeFilter;
	public static void main (String[]args) {
		Employee = new ArrayList<>();
		Employee = readData();
		
		sortName();
	}
	public static void sortName() {
		Employee.sort((o1,o2) -> o1.getEMP_Name().compareTo(o2.getEMP_Name()));
		for(Employee e: Employee) {
			System.out.println(e);
		}
	}
	public static void sortBirth() {
		Employee.sort((o1,o2) -> o1.getBirthdate().compareTo(o2.getBirthdate()));
		for(Employee e: Employee) {
			System.out.println(e);
		}
	}
	public static void sortBekasiName() {
		employeeFilter = Employee.stream()
						.filter(emp -> emp.city.equals("BEKASI"))
						.collect(Collectors.toList());
		employeeFilter.sort((o1,o2) -> o1.getEMP_Name().compareTo(o2.getEMP_Name()));
		for(Employee e : employeeFilter) {
			System.out.println(e);
		}
	}
	public static List<Employee> readData(){
		String file = "C:\\Users\\Alfian Sa'dan M\\workspace\\Chapter4\\ABC.txt";
		List<Employee> data = new ArrayList<>();
		Employee emp = new Employee();
		try(BufferedReader br = new BufferedReader(new FileReader(file))){
			String line = "";
			String[] val;
			br.readLine();
			while((line = br.readLine()) != null) {
				val = line.split(",");
				emp.setEMP_ID(Integer.parseInt(val[0]));
				emp.setEMP_Name(val[1]);
				emp.setGender(val[2]);
				emp.setBirthdate(val[3]);
				emp.setADDRESS(val[4]);
				emp.setCity(val[5]);
				data.add(new Employee(emp.getEMP_ID(),emp.getEMP_Name(), emp.getGender()
						, emp.getBirthdate(), emp.getADDRESS(),emp.getCity()));
			}
			}catch (FileNotFoundException e) {
				e.printStackTrace();
			}catch(IOException e) {
				e.printStackTrace();
			}
			return data;
		}
	}

