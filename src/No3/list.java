package No3;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.awt.event.ActionEvent;
import java.util.Collections;


public class list {

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	LinkedList<Integer>list1;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					list window = new list();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public list() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(134, 23, 173, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(38, 67, 346, 97);
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		
		list1 = new LinkedList<Integer>();
//		Action action = new AbstractAction()ActionListener
		JButton btnAction = new JButton("Action");
		btnAction.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
							int num = Integer.parseInt(textField.getText());
							textField.setText("");
							boolean dup = false;
							for(int i= 0; i< list1.size(); i++) {
								if(list1.get(i)== num) {
									dup = true;
								}
							}
							if(dup == false) {
								list1.add(num);
							}
							textField_1.setText(list1.toString());
				}catch(Exception ex) {
						
						JOptionPane.showMessageDialog(null, "Enter valid number");
				
			}}}
		);
//		textField.addActionListener(Action);
		
		btnAction.setBounds(317, 22, 89, 23);
		frame.getContentPane().add(btnAction);
		
		
		JButton btnNewButton = new JButton("Sort");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			Collections.sort(list1);
			textField_1.setText(list1.toString());
			}
		});
		
		btnNewButton.setFont(new Font("Sitka Subheading", Font.BOLD, 12));
		btnNewButton.setBounds(38, 191, 89, 23);
		frame.getContentPane().add(btnNewButton);
		
		JButton btnSuffle = new JButton("Suffle");
		btnSuffle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Collections.shuffle(list1);
				textField_1.setText(list1.toString());
			}
		});
		btnSuffle.setFont(new Font("Sitka Subheading", Font.BOLD, 12));
		btnSuffle.setBounds(160, 190, 89, 23);
		frame.getContentPane().add(btnSuffle);
		
		JButton btnReserve = new JButton("Reverse");
		btnReserve.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Collections.reverse(list1);
				textField_1.setText(list1.toString());
			}
		});
		btnReserve.setFont(new Font("Sitka Subheading", Font.BOLD, 12));
		btnReserve.setBounds(296, 190, 89, 23);
		frame.getContentPane().add(btnReserve);
		
		JLabel lblMasu = new JLabel("Masukkan angka");
		lblMasu.setBounds(10, 26, 93, 14);
		frame.getContentPane().add(lblMasu);
		

	}
}
